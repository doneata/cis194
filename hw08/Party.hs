module Party where

import Data.Monoid
import Data.Tree
import Employee

-- Exercise 1.
glCons :: Employee -> GuestList -> GuestList
glCons e (GL es ff) = GL (e:es) (ff + empFun e)

instance Monoid GuestList where
    mempty                          = GL [] 0
    mappend (GL es1 f1) (GL es2 f2) = GL (es1 ++ es2) (f1 + f2)

moreFun :: GuestList -> GuestList -> GuestList
moreFun gl1@(GL _ f1) gl2@(GL _ f2) | f1 >= f2 = gl1
                                    | f1 <  f2 = gl2

-- Exercise 2.
treeFold :: (a -> [b] -> b) -> b -> Tree a -> b
treeFold f e (Node {rootLabel = root, subForest = forest}) = f root mappedForest
    where mappedForest = map (treeFold f e) forest

-- Exercise 3.
nextLevel :: Employee -> [(GuestList, GuestList)] -> (GuestList, GuestList)
nextLevel boss gls = (glWithBoss', glWithoutBoss')
    where glWithBoss'        = glCons boss $ mconcat glWithoutBoss
          glWithoutBoss'     = mconcat $ map (uncurry $ moreFun) gls
          (_, glWithoutBoss) = unzip gls

-- Exercise 4.
maxFun :: Tree Employee -> GuestList
maxFun = (uncurry $ moreFun) . treeFold nextLevel (emptyGuestList, emptyGuestList)
    where emptyGuestList = mempty :: GuestList

-- Exercise 5.
main :: IO()
main = readFile "company.txt" >>= (putStr . unlines . prepareOutput . read)

prepareOutput :: Tree Employee -> [String]
prepareOutput tree = strFun : names
    where strFun = "Total fun: " ++ show fun
          names = map empName emps
          GL emps fun = maxFun tree

