
-- Splits integer to its digits.
toDigits :: Integer -> [Integer]
toDigits n
    | n < 0    = []
    | otherwise = toDigits (n `div` 10) ++ [n `mod` 10]

-- Splits integer to its digits reversed.
toDigitsRev :: Integer -> [Integer]
toDigitsRev = reverse . toDigits

-- Double every second element in a list beginning from left.
doubleEveryOtherLeft :: [Integer] -> [Integer]
doubleEveryOtherLeft [] = []
doubleEveryOtherLeft (x:[]) = [x]
doubleEveryOtherLeft (x:y:zs) = x : 2 * y : doubleEveryOtherLeft zs

-- Doubles every second element in a list beginning from right.
doubleEveryOtherRight :: [Integer] -> [Integer]
doubleEveryOtherRight = reverse . doubleEveryOtherLeft . reverse

doubleEveryOther = doubleEveryOtherRight

-- Computes sum of all the digits in all the integers.
sumDigits :: [Integer] -> Integer
sumDigits [] = 0
sumDigits (x:xs) = (sum $ toDigits x) + (sumDigits xs)

-- Checks if the credit is valid.
validate :: Integer -> Bool
validate n = mod (sumDigits $ doubleEveryOther $ toDigits n) 10 == 0

