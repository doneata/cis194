{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Calc where

import qualified Data.Map as M

import ExprT
import Parser
import qualified StackVM

-- Exercise 1.
eval :: ExprT -> Integer
eval (Lit n) = n
eval (Add e1 e2) = (eval e1) + (eval e2)
eval (Mul e1 e2) = (eval e1) * (eval e2)

-- Exercise 2.
evalStr :: String -> Maybe Integer
evalStr str = case parseExp Lit Add Mul str of
                  Just expr -> Just (eval expr)
                  Nothing -> Nothing

-- Exercise 3.
class Expr a where
    lit :: Integer -> a
    add :: a -> a -> a
    mul :: a -> a -> a

instance Expr ExprT where
    lit n = Lit n
    add e1 e2 = Add e1 e2
    mul e1 e2 = Mul e1 e2

reify :: ExprT -> ExprT
reify = id

-- Exercise 4.
newtype MinMax = MinMax Integer deriving (Eq, Show)
newtype Mod7   = Mod7 Integer deriving (Eq, Show)

instance Expr Integer where
    lit n = n
    add e1 e2 = e1 + e2
    mul e1 e2 = e1 * e2

instance Expr Bool where
    lit n | n <= 0    = False
          | otherwise = True
    add e1 e2 = e1 || e2
    mul e1 e2 = e1 && e2

instance Expr MinMax where
    lit n = MinMax n
    add (MinMax e1) (MinMax e2) = MinMax (max e1 e2)
    mul (MinMax e1) (MinMax e2) = MinMax (min e1 e2)

instance Expr Mod7 where
    lit n = Mod7 (n `mod` 7)
    add (Mod7 e1) (Mod7 e2) = Mod7 ((e1 + e2) `mod` 7)
    mul (Mod7 e1) (Mod7 e2) = Mod7 ((e1 * e2) `mod` 7)

testExp :: Expr a => Maybe a
testExp = parseExp lit add mul "(3 * -4) + 5"

testInteger = testExp :: Maybe Integer
testBool    = testExp :: Maybe Bool
testMM      = testExp :: Maybe MinMax
testSat     = testExp :: Maybe Mod7

-- Exercise 5.
instance Expr StackVM.Program where
    lit n = [StackVM.PushI n]
    add e1 e2 = e1 ++ e2 ++ [StackVM.Add]
    mul e1 e2 = e1 ++ e2 ++ [StackVM.Mul]

compile :: String -> Maybe StackVM.Program
compile = parseExp lit add mul

-- Exercise 6.
class HasVars a where
    var :: String -> a

data VarExprT = VLit Integer
              | VVar String
              | VAdd VarExprT VarExprT
              | VMul VarExprT VarExprT
  deriving (Show, Eq)

instance Expr VarExprT where
    lit n = VLit n
    add e1 e2 = VAdd e1 e2
    mul e1 e2 = VMul e1 e2

instance HasVars VarExprT where
    var s = VVar s

-- Helper function to extract the values from Maybe.
f :: (a -> a -> a) -> Maybe a -> Maybe a -> Maybe a
f op (Just x) (Just y) = Just (op x y)
f _ _ _ = Nothing 

instance Expr (M.Map String Integer -> Maybe Integer) where
    lit n = \_ -> Just n
    add e1 e2 = \m -> f (+) (e1 m) (e2 m)
    mul e1 e2 = \m -> f (*) (e1 m) (e2 m)
    
instance HasVars (M.Map String Integer -> Maybe Integer) where
    var s = \m -> M.lookup s m

withVars :: [(String, Integer)] -> (M.Map String Integer -> Maybe Integer) -> Maybe Integer
withVars vs exp = exp $ M.fromList vs

