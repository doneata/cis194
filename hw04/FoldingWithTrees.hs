
data Tree a = Leaf
            | Node Integer (Tree a) a (Tree a)
    deriving (Show, Eq)

-- First variant, it does not use `foldr`.
getDepth :: [a] -> Integer
getDepth = toInteger . ceiling . logBase 2 . fromIntegral . (+1) . length

split :: [a] -> ([a], [a])
split [] = ([],[])
split [x] = ([x],[])
split (x:y:zs) = let (xs, ys) = split zs in (x:xs, y:ys)

foldTree :: [a] -> Tree a
foldTree [] = Leaf
foldTree elems@(x:xs) = Node depth (foldTree ys) x (foldTree zs)
    where depth = (getDepth elems) - 1
          (ys, zs) = split xs

-- Second variant, it does use `foldr`.
getDepth' :: Tree a -> Integer
getDepth' (Leaf) = 0
getDepth' (Node depth _ _ _) = depth

countNodes :: Tree a -> Int
countNodes Leaf = 0
countNodes (Node _ left _ right) = 1 + countNodes left + countNodes right

isComplete :: Tree a -> Bool
isComplete Leaf = True
isComplete tree = nrNodes == nrNodesComplete
    where
        nrNodes = fromIntegral $ countNodes tree
        nrNodesComplete = subtract 1 $ (2**) $ fromIntegral $ (1+) $ getDepth' tree

insert :: a -> Tree a -> Tree a
insert x Leaf = Node 0 Leaf x Leaf
insert x (Node depth left r right)
    | nodesLeft <= nodesRight = Node (depth + inc) (insert x left) r right
    | nodesLeft >  nodesRight = Node depth left r (insert x right)
    where
        nodesLeft  = countNodes left
        nodesRight = countNodes right
        inc = if isComplete left then 1 else 0

foldTree' :: [a] -> Tree a
foldTree' = foldr insert Leaf

