
sieveSundaram :: Integer -> [Integer]
sieveSundaram n = map (\x -> 2 * x + 1) $ filter (\x -> not $ elem x blacklist) [1..n]
    where blacklist = [i + j + 2 * i * j | j <- [1..(n `div` 2)], i <- [1..j]]

