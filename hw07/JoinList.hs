{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-missing-methods #-}

import Data.Monoid

import Buffer
import Editor
import Scrabble
import Sized

data JoinList m a = Empty
                  | Single m a
                  | Append m (JoinList m a) (JoinList m a)
    deriving (Eq, Show)

-- Helper functions used for testing.
(!!?) :: [a] -> Int -> Maybe a
[]     !!? _         = Nothing
_      !!? i | i < 0 = Nothing
(x:xs) !!? 0         = Just x
(x:xs) !!? i         = xs !!? (i-1)

jlToList :: JoinList m a -> [a]
jlToList (Empty)               = []
jlToList (Single _ a)          = [a]
jlToList (Append _ left right) = jlToList left ++ jlToList right

-- Exercise 1.
(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
(+++) x y = Append ((tag x) `mappend` (tag y)) x y

tag :: Monoid m => JoinList m a -> m
tag (Empty)        = mempty
tag (Single m _)   = m
tag (Append m _ _) = m

-- Exercise 2.
getSizeJ :: (Sized b, Monoid b) => JoinList b a -> Int
getSizeJ = getSize . size . tag

indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a
indexJ _ Empty                   = Nothing
indexJ i _              | i <  0 = Nothing
indexJ i (Single _ a)   | i >  0 = Nothing
indexJ 0 (Single _ a)            = Just a
indexJ i (Append _ x y) | i <  s = indexJ i x
                        | i >= s = indexJ (i - s) y
    where s = getSizeJ x

dropJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a 
dropJ _ Empty                   = Empty
dropJ 1 (Single _ _)            = Empty
dropJ i jl             | i <= 0 = jl
dropJ i (Append _ x y) | i <  s = dropJ i x +++ y
                       | i >= s = dropJ (i - s)  y
    where s = getSizeJ x

takeJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
takeJ i _              | i <= 0 = Empty
takeJ _ Empty                   = Empty
takeJ 1 x@(Single _ _)          = x
takeJ i (Append _ x y) | i <  s = takeJ i x
                       | i >= s = x +++ takeJ (i - s) y
    where s = getSizeJ x

-- Exercise 3.
scoreLine :: String -> JoinList Score String
scoreLine n = Single (scoreString n) n

-- Exercise 4.
split :: [a] -> ([a], [a])
split lst = splitAt (((length lst) + 1) `div` 2) lst

instance Buffer (JoinList (Score, Size) String) where
    toString (Empty)        = ""
    toString (Single _ a)   = a
    toString (Append _ x y) = toString x ++ toString y

    fromString = f . lines
        where f []  = Empty
              f [s] = Single (scoreString s, Size 1) s
              f ss  = f xs +++ f ys
                  where (xs, ys) = split ss

    line              = indexJ
    replaceLine n l b = takeJ n b +++ (Single (scoreString l, Size 1) l) +++ dropJ (n + 1) b
    numLines          = getSizeJ
    value b           = score where (Score score, _) = tag b

jl :: JoinList (Score, Size) String
jl = fromString $ unlines
         [ "This buffer is for notes you don't want to save, and for"
         , "evaluation of steam valve coefficients."
         , "To load a different file, type the character L followed"
         , "by the name of the file."
         ]

main = runEditor editor jl

