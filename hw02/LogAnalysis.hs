{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where

import Log

parseMessageWords :: [String] -> LogMessage
parseMessageWords ("I":t:xs) = LogMessage Info (read t :: Int) (unwords xs)
parseMessageWords ("W":t:xs) = LogMessage Warning (read t :: Int) (unwords xs)
parseMessageWords ("E":n:t:xs) = LogMessage (Error (read n :: Int)) (read t :: Int) (unwords xs)
parseMessageWords s = Unknown (unwords s)

parseMessage :: String -> LogMessage
parseMessage = parseMessageWords . words

parse :: String -> [LogMessage]
parse = (map parseMessage) . lines

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert mx Leaf = Node Leaf mx Leaf
insert mx (Node left my right) =
    if tx < ty
    then Node (insert mx left) my right
    else Node left my (insert mx right)
    where LogMessage _ tx _ = mx
          LogMessage _ ty _ = my

build :: [LogMessage] -> MessageTree
build [] = Leaf
build (m:ms) = insert m (build ms)

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left m right) = inOrder left ++ [m] ++ inOrder right

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = (map getMessage) . (filter isSevereError) . inOrder . build
    where isSevereError (LogMessage (Error e) _ _) = e > 50
          isSevereError _ = False
          getMessage (LogMessage _ _ m) = m

