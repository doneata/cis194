{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-missing-methods #-}

-- Exercise 1.
fib :: Integer -> Integer
fib 0          = 0
fib 1          = 1
fib n | n >= 2 = fib (n - 1) + fib (n - 2)

fibs1 :: [Integer]
fibs1 = map fib [0..]

-- Exercise 2.
fib2 :: Integer -> Integer
fib2 n | n >= 0 = f n 0 1
    where f 0 a _ = a
          f i a b = f (i - 1) b (a + b)

fibs2 :: [Integer]
fibs2 = map fib2 [0..]

-- Exercise 3.
data Stream a = Cons a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Cons x s) = x : streamToList s

instance Show a => Show (Stream a) where
    show = show . take 20 . streamToList

-- Exercise 4.
streamRepeat :: a -> Stream a
streamRepeat x = Cons x (streamRepeat x)

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons x s) = Cons (f x) (streamMap f s)

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f seed = Cons seed (streamFromSeed f (f seed))

-- Exercise 5.
nats :: Stream Integer
nats = streamFromSeed (+1) 0

interleaveStreams :: Stream a -> Stream a -> Stream a
interleaveStreams = interleaveStreamsL
    where interleaveStreamsL (Cons x xs) yy = Cons x (interleaveStreamsR xs yy)
          interleaveStreamsR xx (Cons y ys) = Cons y (interleaveStreamsL xx ys)

ruler :: Stream Integer
ruler = f 0
    where f n = interleaveStreams (streamRepeat n) (f $ n + 1)

-- Exercise 6.
x :: Stream Integer
x = Cons 0 (Cons 1 (streamRepeat 0))

instance Num (Stream Integer) where
    fromInteger n                = Cons n (streamRepeat 0)
    negate                       = streamMap negate
    (Cons x xs) +    (Cons y ys) = Cons (x + y) (xs + ys)
    (Cons x xs) * yy@(Cons y ys) = Cons (x * y) (streamMap (*x) ys + xs * yy)

instance Fractional (Stream Integer) where
    xx@(Cons x xs) / yy@(Cons y ys) = Cons (x `div` y)
                                           (streamMap (`div` y) (xs - xx * ys / yy))

fibs3 :: Stream Integer
fibs3 = x / (1 - x - x^2)

-- Exercise 7.
data Matrix = Matrix Integer Integer Integer Integer
    deriving Show

instance Num Matrix where
    (Matrix x11 x12 x21 x22) * (Matrix y11 y12 y21 y22) = Matrix
        (x11 * y11 + x12 * y21)
        (x11 * y12 + x12 * y22)
        (x21 * y11 + x22 * y21)
        (x21 * y12 + x22 * y22)

fib4 :: Integer -> Integer
fib4 0 = 0
fib4 1 = 1
fib4 n = let Matrix x _ _ _ = (Matrix 1 1 1 0)^(n - 1) in x

fibs4 :: [Integer]
fibs4 = map fib4 [0..]

