module Golf where

import Data.List

skips :: [a] -> [[a]]
skips x = [[x !! (i-1) | i <- [n,2*n..t]] | n <- [1..t]] where
      t = length x

localMaxima :: [Integer] -> [Integer]
localMaxima x = map (snd . fst) $
                filter (\((a, b), c) -> a < b && b > c) $
                zip (zip x $ drop 1 x) (drop 2 x)

histogram :: [Integer] -> String
histogram x = unlines $ transpose $ map f [0..9] where
            m = maximum $ map length $ group $ sort x
            f i = replicate (m - c) ' ' ++ replicate c '*' ++ "=" ++ show i
            where c = length $ filter (== i) x

