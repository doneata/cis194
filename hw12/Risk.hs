{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Risk where

import Control.Monad.Random
import Data.List (sort)

------------------------------------------------------------
-- Die values

newtype DieValue = DV { unDV :: Int } 
  deriving (Eq, Ord, Show, Num)

first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

instance Random DieValue where
  random           = first DV . randomR (1,6)
  randomR (low,hi) = first DV . randomR (max 1 (unDV low), min 6 (unDV hi))

die :: Rand StdGen DieValue
die = getRandom

------------------------------------------------------------
-- Risk

type Army = Int

data Battlefield = Battlefield { attackers :: Army, defenders :: Army }

battle :: Battlefield -> Rand StdGen Battlefield
battle b = sequence (replicate m die) >>= \attDice ->
           sequence (replicate n die) >>= \defDice ->
           return (battleDeterministic b attDice defDice)
           where m = attackers b
                 n = defenders b

battleDeterministic :: Battlefield -> [DieValue] -> [DieValue] -> Battlefield
battleDeterministic b attDice defDice = Battlefield (attackers b - attDead) (defenders b - defDead)
    where attDead    = length $ filter (\(x, y) -> x - y <= 0) headToHead
          defDead    = length attDice - attDead
          headToHead = zip (reverse $ sort attDice) (reverse $ sort defDice)

invade :: Battlefield -> Rand StdGen Battlefield
invade b = battle b >>= \x ->
    if (attackers x > 2 || defenders x > 0)
    then invade x
    else return b

successProb :: Battlefield -> Rand StdGen Double
successProb b = sequence (replicate 1000 $ invade b) >>= \bs ->
                return ((fromIntegral $ length $ filter (\bb -> defenders bb == 0) bs) / 1000.0)

